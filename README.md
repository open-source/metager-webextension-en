# The official WebExtension for the German search engine MetaGer.

Copyright © SUMA-EV and contributors

## Build the extension yourself (Firefox)

We are using webpack to provide the necessary libraries for this extension. You will need to have [nodejs v20](https://nodejs.org/en/download) installed on your system for the build.

### Debug using nodejs

#### Build the js files (Firefox):
```bash
npm run dev-ff
```
#### Debug the extension in your browser (Firefox)
```bash
npm run dev-ff-run
```

### Build production using nodejs (Firefox)

#### Build the js files and package the extension under `./dist/firefox/` using nodejs
```bash
npm run prod-ff
```

#### Build the js files (Chrome):
```bash
npm run dev-chrome
```
#### Debug the extension in your browser (Chrome)
1. Open `chrome://extensions` in your browser
2. Click `Load unpacked` and select the root directory of this git repository

### Build production using nodejs (Chrome)

#### Build the js files and package the extension under `./dist/chrome/` using nodejs
```bash
npm run prod-chrome
```

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
