# 1.12

* Fix: NetRequest Rules only applied for intiatorDomains of MetaGer

# v1.2

* Add hint to show if settings are applied in inkognito mode
* Allow fraction Token payments
* Update Anonymous Token payment to be synchronious
* Fix: Interface on permission updates
* Fix: NetRequestRules not getting applied to Chrome startpages

# v0.1.0.3

* Fix: Broken link to help page for homomorphic encryption
* Feature: Better localized links to MetaGer pages
* Fix: Settings failed to save on Firefox for Android in some cases