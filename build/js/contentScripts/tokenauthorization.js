/**
 * When token authorization is used for anonymous usage of
 * a MetaGer Key (https://metager.org/en-US/keys/help/anonymous-token)
 * The user will have to provide enuough anonymous token with his
 * search request to cover the exact cost of the search request.
 * Those cost might vary depending on which search engines are enabled
 * by the user so the server will tell us the exact cost which this content script
 * parses and attaches the exact amount of anonymous token to the request before
 * proceeding with the search.
 */

if (typeof browser == "undefined") {
    var browser = chrome;
}

let payment_input = document.getElementById("payment");
let error_url_input = document.getElementById("error_url");
let form = document.getElementById("form");

if (payment_input && form && error_url_input) {
    let payment_request = JSON.parse(atob(payment_input.value));
    browser.runtime.sendMessage({ type: "tokenauthorization", action: "pay", payment_request: payment_request }).then(answer => {
        if (answer.status == "ok") {
            let payment_id_input = document.createElement("input");
            payment_id_input.type = "hidden";
            payment_id_input.id = "anonymous-token-payment-id";
            payment_id_input.name = "anonymous-token-payment-id";
            payment_id_input.value = payment_request.payment_id;
            form.appendChild(payment_id_input);
            let payment_uid_input = document.createElement("input");
            payment_uid_input.type = "hidden";
            payment_uid_input.id = "anonymous-token-payment-uid";
            payment_uid_input.name = "anonymous-token-payment-uid";
            payment_uid_input.value = payment_request.payment_uid;
            form.appendChild(payment_uid_input);
            form.submit();
        } else {
            console.error(answer);
            document.location.replace(error_url_input.value)
        }
    }).catch(error => {
        console.error(answer);
        document.location.replace(error_url_input.value)
    })
}