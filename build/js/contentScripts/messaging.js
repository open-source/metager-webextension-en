if (typeof browser == "undefined") {
    var browser = chrome;
}
window.addEventListener("message", (event) => {
    if (
        event.source === window &&
        event?.data?.sender === "webpage"
    ) {
        let response_message = {
            sender: "tokenmanager",
            message_id: event.data.message_id,
            payload: null,
            error: null
        };
        browser.runtime.sendMessage(event.data.payload).then(response => {
            response_message.payload = response;
            window.postMessage(response_message);
        }).catch(error => {
            response_message.error = error;
            window.postMessage(response_message);
        });
    }
});