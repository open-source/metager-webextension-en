const { SettingsManager } = require("./SettingsManager");
const { TokenManager } = require("./TokenManager");

let settingsManager = new SettingsManager();
let tokenManager = new TokenManager();

if (typeof browser == "undefined") {
  var browser = chrome;
}

urls = [];
origins = [];
hosts = [];
browser.runtime.getManifest().host_permissions.forEach((value, index) => {
  urls.push(value);
  let url = new URL(value);
  hosts.push(url.host);
  origins.push(url.origin);
});


/**
 * Create alarm for updating token status every 15 minutes
 */
browser.alarms.create("token_status_updater", { periodInMinutes: 15 });
browser.alarms.onAlarm.addListener(async alarm => {
  if (alarm.name == "token_status_updater") {
    return tokenManager.getKeyCharge();
  }
})

/**
 * Execute things when a new browser profile is created
 */
browser.runtime.onStartup.addListener(async () => {
  // Initialize our settings managers
  if (BROWSER != "CHROME") return;
  /** Chrome currently does not apply NetRequestRules to startpages. But they will be applied after a reload */
  return browser.tabs.query({}).then(tabs => {
    let reloads = [];
    tabs.forEach(tab => reloads.push(browser.tabs.reload(tab.id)));
    return Promise.all(reloads);
  })
});


/**
 * Handle messages between content scripts and background script:
 * 1. Handles events where the user changed search settings so they
 *    can be synchronized with the extension storage.
 * 2. Handle attaching anonymous tokens to the search request of the user
 *    so the request can be properly authorized.
 */
browser.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (!request.hasOwnProperty("type")) return;
  if (request.type == "settings_set" && request.hasOwnProperty("settings")) {
    if (request.settings.hasOwnProperty("key")) {
      tokenManager.store_key(request.settings.key);
      delete request.settings.key;
    }
    settingsManager.set(request.settings).then(() => {
      sendResponse({ status: "ok" });
    }).catch(error => {
      console.trace(error);
      sendResponse({ status: "error" });
    });
    return true;
  }
  if (request.type == "settings_remove" && request.hasOwnProperty("setting_key")) {
    if (request.setting_key == "key") {
      tokenManager.store_key(null).then(() => {
        sendResponse({ status: "ok" });
      }).catch(error => {
        console.trace(error);
        sendResponse({ status: "error" });
      });
    } else {
      settingsManager.remove(request.setting_key).then(() => {
        sendResponse({ status: "ok" });
      }).catch(error => {
        console.trace(error);
        sendResponse({ status: "error" });
      });
    }
    return true;
  }
  if (request.type == "key") {
    if (request.hasOwnProperty("action") && request.action == "getcharge") {
      tokenManager.getKeyCharge().then(charge => {
        sendResponse({ status: "ok", data: { charge: charge } });
      }).catch(error => {
        console.trace(error);
        sendResponse({ status: "error" });
      });
      return true;
    }
  }
  if (request.type == "tokenauthorization") {
    if (request.hasOwnProperty("action")) {
      if (request.action == "pay") {
        tokenManager.pay_payment_request(request.payment_request).then(() => {
          sendResponse({ status: "ok" });
        }).catch(error => {
          console.error(error);
          sendResponse({ status: "error", msg: error });
        });
        return true;
      } else if (request.action == "gettoken") {
        tokenManager.getTokens(request.payment_request.missing).then(tokens => {
          tokens.tokens.forEach(token => {
            request.payment_request.tokens.tokens.push(token);
          });
          tokens.decitokens.forEach(token => {
            request.payment_request.tokens.decitokens.push(token);
          });
          sendResponse(request.payment_request);
        }).then(() => tokenManager.store_tokens())
          .catch(error => {
            console.error(error);
            sendResponse(request.payment_request);
          })
        return true;
      } else if (request.action == "puttoken") {
        tokenManager.putTokens(request.payment_request)
          .then(() => tokenManager.store_tokens()).then(() => {
            request.payment_request.tokens.tokens = [];
            request.payment_request.tokens.decitokens = [];
            sendResponse(request.payment_request);
          })
          .catch(error => {
            console.error(error);
            sendResponse(request.payment_request);
          })
        return true;
      }
    }
  }
});

/**
 * When the user enables/disables storage of settings within this
 * web-extension we'll grab all existing settings and put it into
 * the web-extension or vise versa.
 * Those settings will also be disabled if the user removes the necessary
 * host permissions from the extension or enabled if they get externally granted
 * and the user already enabled storage of the settings
 */
browser.storage.onChanged.addListener(async (changes, areaName) => {
  settingsManager.handleStorageChange(changes);
  tokenManager.handleStorageChange(changes);
});
browser.permissions.onAdded.addListener(async (permissions) => {
  browser.runtime.reload();
  return;
});
browser.permissions.onRemoved.addListener(async (permissions) => {
  browser.runtime.reload();
  return;
});

/**
 * Make sure we also catch changes in search settings when they
 * are added i.e. using the dev tools or the setting cookies
 * are getting restored in other ways.
 */
browser.cookies.onChanged.addListener(async changeInfo => {
  await settingsManager.handleCookieChange(changeInfo);
  await tokenManager.handleCookieChange(changeInfo);
})

/**
 * Alternatively catch changes in search settings by parsing the
 * cookies in response headers to make sure the webextensions
 * stores all search settings consistently.
 * 
 * Headers will not be modified in those listeners.
 */
// Handle Request Headers
let extraInfoSpecRequest = ["requestHeaders"];
if (BROWSER == "CHROME") {
  extraInfoSpecRequest.push("extraHeaders");
}
browser.webRequest.onSendHeaders.addListener(
  async (details) => {
    await tokenManager.handleRequestHeaders(details);
  },
  {
    urls: origins.map(origin => origin + "/*"),
  },
  extraInfoSpecRequest
);
// Handle Response Headers
let extraInfoSpecResponse = ["responseHeaders"];
if (BROWSER == "CHROME") {
  extraInfoSpecResponse.push("extraHeaders");
}
browser.webRequest.onHeadersReceived.addListener(
  (details) => {
    settingsManager.handleResponseHeaders(details);
    tokenManager.handleResponseHeaders(details);
  },
  {
    urls: origins.map(origin => origin + "/*"),
  },
  extraInfoSpecResponse
);

/**
 * Open settings page when user installs the addon
 * as additional permissions might be required to use
 * all features of this extension.
 * Opening the page on update is not required if the user comes from
 * a version that already has a settings page included
 */
browser.runtime.onInstalled.addListener(details => {
  let open_settings_page = false;

  if (details.reason == "update" && details.previousVersion == "0.0.1.3") {
    open_settings_page = true;
  }

  if (details.reason == "install") {
    open_settings_page = true;
  }

  if (details.temporary == true) {
    open_settings_page = false;
  }

  if (open_settings_page) {
    browser.runtime.openOptionsPage();
  }
})