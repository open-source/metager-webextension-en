import NodeRSA from "node-rsa";
import { Cookie } from "./Cookie";
import { BlindSignature } from "./BlindSignature";

if (typeof browser == "undefined") {
  var browser = chrome;
}
/**
 * If the user is using a MetaGer Key (https://metager.org/keys)
 * to use our search engine without advertisements he can decide
 * to use anonymous token (https://metager.org/keys/help/anonymous-token)
 * instead of his key to authorize his requests to our server.
 * 
 * Homomorphic encryption is used to generate signed authorization tokens
 * which are then impossible for us to connect with the used MetaGer
 * Key.
 * 
 * The Tokenmanager is responsible for making sure that the configured MetaGer
 * Key is no longer transmitted to our server. Also it will generate anonymous token
 * as required and attach them to the search requests as needed.
 */
export class TokenManager {
  _storage_keys = {
    anonymous_tokens_enabled: "use_anonymous_tokens",
    key: "key",
    key_charge: "anonymous_tokens_key_charge",
    tokens: "anonymous_tokens",
    decitokens: "anonymous_decitokens",
    tokens_recent_costs: "anonymous_tokens_recent_costs",
    settings: "settings_settings",
  };

  _handled_cookies = ["key", "cost", "tokens"]
  _permissions_granted = false;

  _initialized = null;
  _anonymous_tokens_enabled = false;
  _key = null;
  _key_charge = 0;
  _payment_id = null;
  _token_target_interval_ms = 3600 * 1000; // How often do we target to recharge tokens => 1hour
  _tokens = {
    tokens: [],
    last_refill: null,  // Timestamp of last refill
    target_amount: 0,   // How much token to store currently
    last_cost: 0        // How much did the latest payment cost
  }; // Stored anonymous Tokens
  _deci_tokens = {
    tokens: [],
    last_refill: null,
    target_amount: 0,
    last_cost: 0
  }; // Stored anonymous Decitokens

  _api_base = "https://metager.de/keys/api/json";

  _urls = [];
  _origins = [];
  _hosts = [];


  /** @type {Promise<null>|null} */
  _refill_promise = null; // Contains eventually already running refill promises

  constructor() {
    browser.runtime.getManifest().host_permissions.forEach((value, index) => {
      this._urls.push(value);
      let url = new URL(value);
      this._hosts.push(url.hostname);
      this._origins.push(url.origin);
    });
    if (process.env.NODE_ENV == "development") {
      this._api_base = "http://localhost:8080/keys/api/json";
    }
    this.init();
  }

  async init() {
    if (this._initialized) return this._initialized;
    // Initialize Settings
    let settings = {};
    settings[this._storage_keys.anonymous_tokens_enabled] = null;
    settings[this._storage_keys.key] = null;

    this._initialized = browser.storage.sync
      .get(settings)
      .then(async (storage) => {
        this._permissions_granted = await this._checkPermissions();
        if (this._permissions_granted == false) {
          this._anonymous_tokens_enabled = false;
        } else if (storage[this._storage_keys.anonymous_tokens_enabled] == null) {
          this._anonymous_tokens_enabled = true;
          let new_value = {};
          new_value[this._storage_keys.anonymous_tokens_enabled] = true;
          await browser.storage.sync.set(new_value);
        } else {
          this._anonymous_tokens_enabled =
            storage[this._storage_keys.anonymous_tokens_enabled];
        }
        await this._load_key_from_browser();
        this._key = storage[this._storage_keys.key];
        settings = {};
        settings[this._storage_keys.tokens] = {
          tokens: [],
          last_refill: null,
          target_amount: 0,
          last_cost: 0
        };
        settings[this._storage_keys.decitokens] = {
          tokens: [],
          last_refill: null,
          target_amount: 0,
          last_cost: 0
        };
        settings[this._storage_keys.tokens_recent_costs] = null;  // ToDo: remove
        settings[this._storage_keys.key_charge] = 0;
        return browser.storage.local.get(settings);
      })
      .then(async (storage) => {
        if (Array.isArray(storage[this._storage_keys.tokens])) {
          // Phase out old storage method of tokens. We are now using an object containing
          // more information instead of an array containing just the stored tokens
          // 06.11.2024 => ToDo: remove
          this._tokens.tokens = storage[this._storage_keys.tokens];
        } else {
          this._tokens = storage[this._storage_keys.tokens];
          this._deci_tokens = storage[this._storage_keys.decitokens];
        }
        // Phase out this storage object if it is defined we will delete it 
        // 06.11.2024 => ToDo: remove
        if (storage[this._storage_keys.tokens_recent_costs] != null) {
          await browser.storage.local.remove(this._storage_keys.tokens_recent_costs);
        }
        this._key_charge = storage[this._storage_keys.key_charge];
        return this.refill();
      }).then(() => this._updateNetRequestRules());
    return this._initialized;
  }

  async handleStorageChange(changes) {
    await this.init();
    let rulesChange = false;
    if (changes.hasOwnProperty(this._storage_keys.anonymous_tokens_enabled)) {
      this._anonymous_tokens_enabled =
        changes[this._storage_keys.anonymous_tokens_enabled].newValue;
      if (this._anonymous_tokens_enabled) await this.refill();
      rulesChange = true;
    }
    if (changes.hasOwnProperty(this._storage_keys.tokens)) {
      if (changes[this._storage_keys.tokens].hasOwnProperty("newValue")) {
        this._tokens = changes[this._storage_keys.tokens].newValue;
      } else {
        this._tokens = {
          tokens: [],
          last_refill: null,
          target_amount: 0,
          last_cost: 0
        };
      }
      rulesChange = true;
    }
    if (changes.hasOwnProperty(this._storage_keys.decitokens)) {
      if (changes[this._storage_keys.decitokens].hasOwnProperty("newValue")) {
        this._deci_tokens = changes[this._storage_keys.decitokens].newValue;
      } else {
        this._deci_tokens = {
          tokens: [],
          last_refill: null,
          target_amount: 0,
          last_cost: 0
        };
      }
      rulesChange = true;
    }
    if (changes.hasOwnProperty(this._storage_keys.key)) {
      let new_key = changes[this._storage_keys.key].newValue;
      this._key = new_key;
      rulesChange = true;
    }
    if (changes.hasOwnProperty(this._storage_keys.key_charge)) {
      this._key_charge = changes[this._storage_keys.key_charge].newValue;
      rulesChange = true;
    }
    if (rulesChange) {
      return this._updateNetRequestRules();
    }
  }

  async handleRequestHeaders(details) {
    await this.init();
    let header_index = null;
    if (details.requestHeaders && details.requestHeaders.some((e, index) => {
      header_index = index;
      return e.name == 'anonymous-token-payment-id';
    })) {
      return this._updateNetRequestRules()
        .then(() => this._fetch_payment_request(details.requestHeaders[header_index].value))
        .then(payment_request => this.pay_payment_request(payment_request))
        .catch(error => console.error(error));
    }

  }

  async handleResponseHeaders(details) {
    await this.init();

    for (let header of details.responseHeaders) {
      if (header.name.match(/set-cookie/i)) {
        let cookies = header.value.split("\n");
        for (let cookie of cookies) {
          let parsed_cookie = new Cookie(cookie);

          if (parsed_cookie.key.match(/(deci)?tokens/i)) {
            if (!parsed_cookie.expired) {
              try {
                let excess_tokens = JSON.parse(parsed_cookie.value);
                for (let excess_token of excess_tokens) {
                  if (parsed_cookie.key == "tokens") {
                    this._tokens.tokens.unshift(excess_token);
                  } else if (parsed_cookie.key == "decitokens") {
                    this._deci_tokens.tokens.unshift(excess_token);
                  }
                }
              } catch (error) {
                console.error(error);
              }
            }
            await this.store_tokens();
          } else if (parsed_cookie.key == "key") {
            if (parsed_cookie.expired) {
              await this.store_key(null);
            } else {
              await this.store_key(parsed_cookie.value);
            }
            await this._updateNetRequestRules();
          }
        }
      }
    }
    return;
  }

  async handleCookieChange(changeInfo) {
    await this.init();
    if (changeInfo.removed) return;  // Do not handle removed cookies
    if (!this._handled_cookies.includes(changeInfo.cookie.name)) return; // Do not handle unspecified cookies
    let protocol = changeInfo.cookie.secure ? "https" : "http";
    return browser.cookies.remove({
      name: changeInfo.cookie.name,
      url: `${protocol}://${changeInfo.cookie.domain}${changeInfo.cookie.path}`
    });
  }

  /**
   * Returns the current key charge
   */
  async getKeyCharge() {
    await this.init();
    return this._fetch_key_charge();
  }

  async getTokens(cost) {
    await this.init();
    return this.refill(cost)
      .then(async () => {
        let tokens = {
          tokens: [],
          decitokens: []
        };
        if (this._anonymous_tokens_count() < cost) {
          throw new Error("I was not able to attach the required token amount to the request");
        }
        // Use up available decitoken if they are not in use anymore => Attach 10 decitoken instead of one token
        if (Number.isInteger(cost) && cost >= 1 && this._deci_tokens.target_amount == 0 && this._deci_tokens.tokens.length >= 10) {
          for (let i = 0; i < 10; i++) {
            let token = this._deci_tokens.tokens.shift();
            if (token == undefined) {
              break;
            }
            tokens.decitokens.push(token);
            cost = parseFloat((cost - 0.1).toFixed(1));
          }
        }
        // Attach as many token as necessary
        while (cost >= 1) {
          let token = this._tokens.tokens.shift();
          if (token == undefined) {
            break;
          }
          tokens.tokens.push(token);
          cost -= 1;
        }
        // Attach as many decitoken as necessary
        while (cost > 0) {
          let token = this._deci_tokens.tokens.shift();
          if (token == undefined) {
            break;
          }
          tokens.decitokens.push(token);
          cost = parseFloat((cost - 0.1).toFixed(1));
        }
        // If we need more tokens attach more regular tokens
        while (cost > 0) {
          let token = this._tokens.tokens.shift();
          if (token == undefined) {
            break;
          }
          tokens.tokens.push(token);
          cost -= 1;
        }

        if (cost > 0) {
          tokens.tokens.forEach(token => {
            this._tokens.tokens.unshift(token);
          });
          tokens.tokens = [];
          tokens.decitokens.forEach(token => {
            this._deci_tokens.tokens.unshift(token);
          });
          tokens.decitokens = [];
          throw new Error("I was not able to attach the required token amount to the request");
        }
        return this.store_tokens().then(() => tokens);
      });
  }

  async putTokens(payment_request) {
    payment_request.tokens.tokens.forEach(token => {
      let duplicate = false;
      this._tokens.tokens.forEach(existing_token => {
        if (token.token == existing_token.token) {
          duplicate = true;
        }
      });
      if (!duplicate) {
        this._tokens.tokens.unshift(token);
      }
    });
    payment_request.tokens.decitokens.forEach(token => {
      let duplicate = false;
      this._deci_tokens.tokens.forEach(existing_token => {
        if (token.token == existing_token.token) {
          duplicate = true;
        }
      });
      if (!duplicate) {
        this._deci_tokens.tokens.unshift(token);
      }
    });
  }

  async _load_key_from_browser() {
    if (!this._permissions_granted) return;
    // Consume a possible key cookie
    let key = "";
    let cookie_promises = [];
    for (let i = 0; i < this._origins.length; i++) {
      let origin = this._origins[i];
      let key_cookie = await browser.cookies.get({ name: "key", url: origin });

      if (key_cookie != null) {
        key = key_cookie.value;
        cookie_promises.push(browser.cookies.remove({ name: "key", url: origin }));
      }
    }
    return Promise.all(cookie_promises).then(() => {
      if (key.length > 0) {
        return this.store_key(key);
      }
    }).catch(error => console.error(error));
  }

  async _fetch_key_charge() {
    if (this._key == null) return 0;

    return fetch(`${this._api_base}/key/` + encodeURIComponent(this._key), {
      headers: {
        "Cache-Control": "no-cache",
      }
    }).then(async response => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error("Couldn't fetch key status");
      }
    }).then(async response => {
      let settings = {};
      settings[this._storage_keys.key_charge] = response.charge;
      return browser.storage.local.set(settings).then(() => response.charge);
    }).catch(error => { console.error(error); return 0 });
  }

  async _updateNetRequestRules() {
    if (!browser.hasOwnProperty("declarativeNetRequest")) return;
    let rules = [];
    if ((this._key && this._anonymous_tokens_enabled && this._key_charge > 0) || this._tokens.tokens.length > 0) {
      if (this._key) {
        // Some paths require a key to be submitted (settings page; key management)
        rules.push({
          id: 2,
          condition: {
            requestDomains: this._hosts,
            urlFilter: "/keys/*",
            resourceTypes: ["main_frame", "sub_frame", "xmlhttprequest"]
          },
          action: {
            type: "modifyHeaders",
            requestHeaders: [{
              header: "key",
              operation: "set",
              value: `${this._key}`
            }]
          }
        });
        rules.push({
          id: 3,
          condition: {
            requestDomains: this._hosts,
            urlFilter: "/meta/settings*",
            resourceTypes: ["main_frame", "sub_frame", "xmlhttprequest"]
          },
          action: {
            type: "modifyHeaders",
            requestHeaders: [{
              header: "key",
              operation: "set",
              value: `${this._key}`
            }]
          }
        });
      }

      rules.push({
        id: 4,
        condition: {
          requestDomains: this._hosts,
          resourceTypes: ["main_frame", "sub_frame", "xmlhttprequest"]
        },
        action: {
          type: "modifyHeaders",
          requestHeaders: [{
            header: "tokenauthorization",
            operation: "set",
            value: this._getTokenAuthorizationStatus()
          },
          {
            header: "tokensource",
            operation: "set",
            value: "webextension"
          }]
        }
      });
      this._payment_id = crypto.randomUUID();
      rules.push({
        id: 5,
        condition: {
          requestDomains: this._hosts,
          urlFilter: "/meta/meta.ger3?*",
          resourceTypes: ["main_frame", "sub_frame", "xmlhttprequest"]
        },
        action: {
          type: "modifyHeaders",
          requestHeaders: [{
            header: "anonymous-token-payment-id",
            operation: "set",
            value: this._payment_id
          }]
        }
      });
    } else if (this._key) {
      rules.push({
        id: 2,
        condition: {
          requestDomains: this._hosts,
          resourceTypes: ["main_frame", "sub_frame", "xmlhttprequest"]
        },
        action: {
          type: "modifyHeaders",
          requestHeaders: [{
            header: "key",
            operation: "set",
            value: `${this._key}`
          }]
        }
      });
    }
    return browser.declarativeNetRequest.updateDynamicRules({
      addRules: rules,
      removeRuleIds: [2, 3, 4, 5]
    });
  }

  async store_key(key) {
    if (key) {
      let settings = {};
      settings[this._storage_keys.key] = key;
      return browser.storage.sync.set(settings).then(async () => this._fetch_key_charge());
    } else {
      return browser.storage.sync.remove(this._storage_keys.key).then(() => browser.storage.local.remove(this._storage_keys.key_charge));
    }
  }

  /**
   * 
   * @param {string} payment_id 
   * @returns {object} payment_request
   */
  async _fetch_payment_request(payment_id) {
    let baseURL = new URL(this._api_base);
    baseURL = new URL(baseURL.origin);
    return fetch(`${baseURL}anonymous-token/cost?anonymous_token_payment_id=${payment_id}`).then(response => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error("Did not receive a valid response");
      }
    }).then(response => {
      if (response.missing <= 0) return Promise.reject(response);
      if (response.async_disabled) throw new Error("Async Payments are temporarily disabled");
      return response;
    })
  }

  /**
   * 
   * @param {*} payment_request 
   */
  async pay_payment_request(payment_request) {
    let baseURL = new URL(this._api_base);
    baseURL = new URL(baseURL.origin);
    return this.refill(payment_request.cost, true)
      .then(() => {
        if (this._anonymous_tokens_count() >= payment_request.cost) {
          // Attach enough anonymous token to the request to pay for
          let cost = payment_request.missing;

          return this.getTokens(cost).then(tokens => {
            payment_request.tokens.tokens = tokens.tokens;
            payment_request.tokens.decitokens = tokens.decitokens;
          });
        } else {
          // Use key instead of token auth or an invalid key to make the request go forward
          if (this._key != null) {
            payment_request.key = this._key;
          } else {
            payment_request.key = "no-key";
          }
        }
      })
      .then(async () => {
        return fetch(`${baseURL}anonymous-token`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(payment_request)
        }).then(async response => {
          switch (response.status) {
            case 202:
              return response.json();
            case 402:
            case 503:
            case 429:
              let response_json = await response.json();
              return Promise.reject(response_json);
            default:
              return Promise.reject(payment_request);
          }
        })
      }).then(response => {
        try {
          response.tokens.tokens.forEach(token => {
            this._tokens.tokens.unshift(token);
          });
          response.tokens.decitokens.forEach(token => {
            this._deci_tokens.tokens.unshift(token);
          });
        } catch (error) {
          console.error(error);
        }
      }).catch(failed_payment => {
        try {
          // Try to recycle tokens. In every handled error case the failed payment will contain tokens we can use again
          failed_payment.tokens.tokens.forEach(token => {
            this._tokens.tokens.unshift(token);
          });
          failed_payment.tokens.tokens = [];
          failed_payment.tokens.decitokens.forEach(decitoken => {
            this._deci_tokens.tokens.unshift(decitoken);
          });
          failed_payment.tokens.decitokens = [];
        } catch (error) {
          console.error(failed_payment);
          console.error(error);
        }
        // This might be a temporary error. We will pay using a key instead
        if (this._key != null && payment_request.missing > 0) {
          payment_request.key = this._key;
          payment_request.tokens.tokens = [];
          payment_request.tokens.decitokens = [];
          return this.store_tokens().then(() => fetch(`${baseURL}anonymous-token`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json"
            },
            body: JSON.stringify(payment_request)
          }));
        }
      }).finally(async () => {
        return this.store_tokens()
          .then(() => this._updateNetRequestRules())
          .then(() => {
            let timeout = Math.floor(Math.random() * (25 - 0 + 1) + 0); // Random number between 0 and 25
            setTimeout(() => this.refill(payment_request.cost), timeout * 1000);
          });
      });
  }

  /**
   * Removes the Cookie Header from header list
   * and returns a parsed cookie object and remaining headers
   * @param {*} requestHeaders
   */
  _parseRequestHeadersForCookies(requestHeaders) {
    let new_headers = [];

    let cookie_header = null;
    for (let header of requestHeaders) {
      if (header.name.match(/cookie/i)) {
        cookie_header = header;
      } else {
        new_headers.push(header);
      }
    }

    let new_cookies = {};
    if (cookie_header) {
      let cookies = cookie_header.value.split(";");
      for (let cookie of cookies) {
        let cookie_array = cookie.split("=");
        if (cookie_array.length != 2) continue;
        let name = cookie_array[0].trim();
        let value = decodeURIComponent(cookie_array[1].trim());
        new_cookies[name] = value;
      }
    }
    return [new_headers, new_cookies];
  }

  /**
   * Removes the MetaGer key from the request
   * and replaces it with the headers for anonymous tokens
   * https://gitlab.metager.de/open-source/MetaGer/-/wikis/Anonymous%20Token%20System
   */
  _getTokenAuthorizationStatus() {
    // Set the tokenauthorization cookie instead of the actual key
    let token_authorization = "empty";
    if (this._key_charge > 30) {
      token_authorization = "full";
    } else if (this._key_charge > 0) {
      token_authorization = "low";
    }
    return token_authorization;
  }

  /**
   * Enable/Disable Tokenmanager when permissions
   * are revoked or granted
   * 
   * @param {*} permissions 
   * @returns 
   */
  async _checkPermissions() {
    let permissionsToRequest = {
      origins: this._urls
    };

    return browser.permissions.contains(permissionsToRequest);
  }

  /**
   * Persists the saved tokens
   */
  async store_tokens(key_charge = null) {
    let new_storage = {};
    if (key_charge != null) {
      new_storage[this._storage_keys.key_charge] = key_charge;
    }
    new_storage[this._storage_keys.tokens] = this._tokens;
    new_storage[this._storage_keys.decitokens] = this._deci_tokens;
    return browser.storage.local.set(new_storage);
  }

  /**
   * @returns {number} The amount of anonymous tokens stored in this extension
   */
  _anonymous_tokens_count() {
    return this._tokens.tokens.length + (this._deci_tokens.tokens.length / 10.0);
  }

  /**
   * Refills locally stored tokens
   */
  async refill(cost = 0, ignore_anonymous_token_state = false) {
    if (this._refill_promise != null) {
      await this._refill_promise;
    }
    if (!this._anonymous_tokens_enabled && ((this._tokens.tokens.length == 0 && this._deci_tokens.tokens.length == 0) || ignore_anonymous_token_state == false)) {
      return; // Do not refill if feature is disabled and no tokens are stored locally
    }
    let min_decitoken = 0;
    let min_token = 0;
    if (cost == 0 && this._anonymous_tokens_enabled) {
      // Load the latest stored cost if none were supplied in this request
      min_decitoken = this._deci_tokens.last_cost;
      min_token = this._tokens.last_cost;
    } else {
      min_decitoken = Math.ceil((cost - Math.floor(cost)) * 10);
      min_token = Math.floor(cost);
    }

    // Do not refill if we can handle the next payment with the same cost
    if (min_token <= this._tokens.tokens.length && min_decitoken <= this._deci_tokens.tokens.length) return;
    if (!this._anonymous_tokens_enabled && this._anonymous_tokens_count() >= cost) return;

    // Reset target amount if too much time has passed
    // 1. Token
    if (this._tokens.last_refill != null && Date.now() - this._tokens.last_refill > (this._token_target_interval_ms * 4)) {
      // First case => reset token target if there was a long pause between requests
      this._tokens.target_amount = 0;
    }
    this._tokens.target_amount = Math.max(this._tokens.target_amount, min_token);
    // 2. Decitoken
    if (this._deci_tokens.last_refill != null && Date.now() - this._deci_tokens.last_refill > (this._token_target_interval_ms * 4)) {
      // First case => reset token target if there was a long pause between requests
      this._deci_tokens.target_amount = 0;
    }
    this._deci_tokens.target_amount = Math.max(this._deci_tokens.target_amount, min_decitoken);

    if (!this._anonymous_tokens_enabled) {
      this._tokens.target_amount = min_token;
      this._deci_tokens.target_amount = min_decitoken;
    }

    let required_token_count = Math.max(this._tokens.target_amount - this._tokens.tokens.length, 0);
    let required_decitoken_count = Math.max(this._deci_tokens.target_amount - this._deci_tokens.tokens.length, 0);

    // Make sure to have enough decitoken available to consume 10 of them instead of a token
    // If we are currently not needing decitoken anymore.
    // This makes sure they are used up at some point
    if (this._deci_tokens.target_amount == 0 && this._deci_tokens.tokens.length > 0 && this._deci_tokens.tokens.length < 10) {
      required_decitoken_count = 10 - this._deci_tokens.tokens.length;
    }

    // Abort if no token need to be charged - This should not be able to happen because of above checks
    if (required_token_count == 0 && required_decitoken_count == 0) return;

    let tokens = [];
    let decitokens = [];
    let token_pubkey = null;
    let decitoken_pubkey = null;
    let key_charge = null;
    this._refill_promise = fetch(this._api_base + "/token/v2/pubkey").then(
      (response) => response.json()
    ).then(response => {
      // Generate new tokens
      token_pubkey = new NodeRSA(response.tokens.pubkey_pem.trim());
      for (let i = 0; i < required_token_count; i++) {
        let token = crypto.randomUUID();
        let blinded = BlindSignature.BLIND({
          message: token,
          key: token_pubkey,
        });
        tokens.push({
          token: token,
          blinded: blinded.blinded,
          blinded_r: blinded.r,
          date: response.tokens.date,
        });
      }
      // Generate new decitokens
      decitoken_pubkey = new NodeRSA(response.decitokens.pubkey_pem.trim());
      for (let i = 0; i < required_decitoken_count; i++) {
        let token = crypto.randomUUID();
        let blinded = BlindSignature.BLIND({
          message: token,
          key: decitoken_pubkey,
        });
        decitokens.push({
          token: token,
          blinded: blinded.blinded,
          blinded_r: blinded.r,
          date: response.decitokens.date,
        });
      }
      // Signing request only with the blinded tokens
      // Rest of the data will be this apps secret
      let request_data = {
        key: this._key,
        blinded_tokens: {
          date: response.tokens.date,
          tokens: tokens.map((token) => token.blinded.toString())
        },
        blinded_decitokens: {
          date: response.decitokens.date,
          tokens: decitokens.map((token) => token.blinded.toString()),
        }
      };
      return fetch(this._api_base + "/token/v2/sign", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(request_data),
      })
    })
      .then(async (response) => {
        if (response.status == 201) {
          return response.json();
        } else {
          // Try to decode the Error message
          try {
            let response_json = await response.json();
            if (response_json.message == "Invalid Key") {
              // The Key is probably empty
              await this.getKeyCharge();
            }
          } catch (ignored) { }
          throw new Error("Error signing tokens. The key is probably empty");
        }
      })
      .then((response) => {
        // Save new key charge
        for (let token of tokens) {
          if (response.signed_tokens.tokens.hasOwnProperty(token.blinded)) {
            let unblinded_signature = BlindSignature.UNBLIND({
              signed: response.signed_tokens.tokens[token.blinded],
              key: token_pubkey,
              r: token.blinded_r,
            });
            if (
              BlindSignature.VERIFY({
                unblinded: unblinded_signature,
                message: token.token,
                key: token_pubkey,
              })
            ) {
              this._tokens.tokens.push({
                token: token.token,
                signature: unblinded_signature.toString(),
                date: token.date,
              });
            }
          }
        }
        for (let token of decitokens) {
          if (response.signed_decitokens.tokens.hasOwnProperty(token.blinded)) {
            let unblinded_signature = BlindSignature.UNBLIND({
              signed: response.signed_decitokens.tokens[token.blinded],
              key: decitoken_pubkey,
              r: token.blinded_r,
            });
            if (
              BlindSignature.VERIFY({
                unblinded: unblinded_signature,
                message: token.token,
                key: decitoken_pubkey,
              })
            ) {
              this._deci_tokens.tokens.push({
                token: token.token,
                signature: unblinded_signature.toString(),
                date: token.date,
              });
            }
          }
        }
        // Update time dependant target amount increase/decrease
        if (tokens.length > 0) {
          if (this._tokens.last_refill != null && Date.now() - this._tokens.last_refill > this._token_target_interval_ms) {
            this._tokens.target_amount -= min_token;
          } else if (this._tokens.last_refill != null && Date.now() - this._tokens.last_refill < this._token_target_interval_ms) {
            this._tokens.target_amount += min_token;
          }
          this._tokens.last_refill = Date.now();
        }
        if (decitokens.length > 0) {
          if (this._deci_tokens.last_refill != null && Date.now() - this._deci_tokens.last_refill > this._token_target_interval_ms) {
            this._deci_tokens.target_amount -= min_decitoken;
          } else if (this._deci_tokens.last_refill != null && Date.now() - this._deci_tokens.last_refill < this._token_target_interval_ms) {
            this._deci_tokens.target_amount += min_decitoken;
          }
          this._deci_tokens.last_refill = Date.now();
        }
        key_charge = response.charge;
        return;
      })
      .catch(error => console.error(error)) // Log any uncaught errors
      .finally(async () => this.store_tokens(key_charge).then(() => this._refill_promise = null)); // Always update 

    return this._refill_promise;
  }
}
